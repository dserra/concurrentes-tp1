#ifndef _IMAGE_HPP_
#define _IMAGE_HPP_


enum ImageStatus { 
    created, 
    ready_process, 
    done 
};


struct Image {
    int id;
    ImageStatus status;
    int* content;
};

#endif