#ifndef _CAMERA_HPP_
#define _CAMERA_HPP_

#include <stdlib.h> 
#include "image.hpp"

class Camera {
public:

    Camera(int size) {
        _size = size;
    }

    void getImageContent(Image* image, int id) {
        image->id = id;
        image->status = created;

        int full_size = _size*_size;

        for(int i=0; i<full_size; i++) {
            image->content[i] = getPixelValue();
        }
    }

private:

    int _size;

    int getPixelValue(){
        return rand() % 10000 + 1;
    }
};

#endif