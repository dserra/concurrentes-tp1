#include <stddef.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

#include <iostream>
#include <vector>
#include <string>

#include "claparser.hpp"
#include "image.hpp"
#include "camera.hpp"

using namespace std;

int main(int argc, char* argv[]) {
    try{
        ClaParser argsParser(argc, argv);
        int images = argsParser.getCamerasParam();
        int size = argsParser.getSizeParam();
        int full_size = size*size;
        int* image_content = (int*)malloc(full_size*sizeof(int));
        Image image;
        image.content = image_content;
        Camera camera(size);
        camera.getImageContent(&image, 1);

        cout << images;
        cout << size;

        for(int i=0; i<full_size; i++) {
            cout << image.content[i];
        }
        // Get Shared Memory
        // Get images
        // Process images
        // Flatten Images
        // Control Signals
        // Create Producers;
        free(image_content);
        
    }catch(const char* str){
        //root.error(str);
    }catch(const std::string & str) {
        //root.error(str);
    }
    return 0;
}

void captureImages(int images) {

}