#ifndef _CLAPARSER_HPP_
#define _CLAPARSER_HPP_

#include <iostream>
#include <algorithm>
#include <string>

class ClaParser {
public:

    ClaParser(int argc, char* argv[]) {
        _argc = argc;
        _argv = argv;

        parse();
    }

    int getCamerasParam() {
        return _cameras;
    }

    int getSizeParam() {
        return _size;
    }

private:

    int _argc;
    char** _argv;
    int _cameras;
    int _size;

    void parse() {
        if(argExists("-h")) {
            showUsage();
            exit(0);
        }
        
        std::string cameras = processArgument("-c", "10");
        std::string size = processArgument("-N", "50");

        _cameras = stoi(cameras);
        _size = stoi(size);
        
    }

    std::string processArgument(const std::string& option, const std::string& default_value){
        if(argExists(option)) {
            return getValue(option); 
        }else{
            return default_value;
        }
    }
        
    std::string getValue(const std::string& option)
    {
        for(int i=0; i<_argc; ++i){
            std::string arg(_argv[i]);
            if ((option.compare(arg) == 0)){
                 std::string result(_argv[i+1]);
                 return result;
            }
        }
    }

    bool argExists(const std::string& option)
    {
        for(int i=0; i<_argc; ++i){
            std::string arg(_argv[i]);
            if ((option.compare(arg) == 0))
                return true;                            
        }
        return false;
    }

    void showUsage()
    {
        std::cout << "[Usage]: " << "ImageProcessor" << " [-h]"
                << " [-c cameras] [-N image_width]\n\n";

        std::cout << "-h," << "  this help\n";
        std::cout << "-c," << "  cameras quantity\n";
        std::cout << "-N," << "  NxN image size\n";

        std::cout << "\nexample: \n" << "    ./ImageProcessor -c 20 -N 50" << std::endl;
    }
};

#endif